
import java.rmi.*;

public class TemperatureClient {

    public TemperatureClient(String ip){

	try {
	    String serverObjectName = "//" + ip + "/TempServer";
	    //pesquisa objeto remoto TemperatureServerImpl no rmiregistry
	    TemperatureServer mytemp = (TemperatureServer) Naming.lookup( serverObjectName );

	    //Invoca o metodo remoto no objeto servidor
	    //que estah registrado no rmiregistry
	    System.out.println(mytemp.metodo2());


	} catch (Exception e){
	    System.out.println("Excecao no cliente: " + e.getMessage());
	    e.printStackTrace();
	}

    }

    public static void main(String [] args){
	if (args.length == 0)
	    new TemperatureClient( "localhost" );
	else
	    new TemperatureClient( args[0] );
    }

}//fim classe