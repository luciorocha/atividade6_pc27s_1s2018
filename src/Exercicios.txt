Exercícios:
0) Faça a clonagem do projeto modelo em:
   https://bitbucket.org/luciorocha/atividade6_pc27s_1s2018
1) No trecho de comentários de cada arquivo .java, informe o seu nome como autor, 
   e atualize a data e hora da última modificação.
2) Modifique o programa servidor para que ele exiba uma mensagem para 
   cada invocação remota do cliente.
3) Implemente mais 3 métodos no programa cliente.
4) Realize as invocações remotas no cliente com os novos métodos.
   Verifique se as requisições-respostas estão corretas.
5) Faça o upload do seu código-fonte no repositório bitbucket.
6) Crie um relatório conciso que contenha:
   6.1) Disciplina: Programação Concorrente e Distribuída - PC27S - 1s2018.   
   6.2) Introdução: explicação do problema abordado
   6.3) Modificações realizadas
   6.4) Resultados 
   6.5) Informe a URL do repositório que contém o seu código-fonte modificado,
        no seguinte formato:
        https://bitbucket.org/<SEU_USUARIO>/atividade6_pc27s_1s2018